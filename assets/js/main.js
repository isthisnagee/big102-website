

$(document).ready(function() {

  $('.q1 .button').click(function(b) {

    $('#q1Chart').fadeIn('slow');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {
      $('.q2').delay(2000).slideDown('slow', function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      });
    });

    var q1Data = [
        {
            value: 33,
            color:"#46BFBD",
            highlight: "#5AD3D1",
            label: "Yes"
        },
        {
            value: 68,
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "No"
        }
    ]

    var ctx = $("#q1Chart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);
    var q1Chart = new Chart(ctx).Pie(q1Data, {});


  });

  $('.q2 .button').click(function(b) {

    $('#q2Chart').fadeIn('slow');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {
      $('.q3').delay(2000).slideDown('slow', function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      });
    });

    var q2Data = [
      {
          value: 9,
          color:"#F7464A",
          highlight: "#FF5A5E",
          label: "<1 hr"
      },
      {
          value: 30,
          color: "#46BFBD",
          highlight: "#5AD3D1",
          label: "1-2 hrs"
      },
      {
          value: 34,
          color: "#FDB45C",
          highlight: "#FFC870",
          label: "2-4 hrs"
      },
      {
          value: 14,
          color: "#5CDFFD",
          highlight: "#70FFF5",
          label: "4-6 hrs"
      },
      {
          value: 14,
          color: "#A770FF",
          highlight: "#9670FF",
          label: ">6 hrs"
      }
    ]

    var ctx = $("#q2Chart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);
    var q2Chart = new Chart(ctx).Pie(q2Data, {});
  });

  $('.q3 .button').click(function(b) {

    $('#q3Chart').fadeIn('slow');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {
      $('.q4').delay(2000).slideDown('slow', function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      });
    });

    var q3Data = [
      {
          value: 11,
          color:"#F7464A",
          highlight: "#FF5A5E",
          label: "1 phone"
      },
      {
          value: 39,
          color: "#46BFBD",
          highlight: "#5AD3D1",
          label: "2 phones"
      },
      {
          value: 30,
          color: "#FDB45C",
          highlight: "#FFC870",
          label: "3 phones"
      },
      {
          value: 13,
          color: "#5CDFFD",
          highlight: "#70FFF5",
          label: "4 phones"
      },
      {
          value: 8,
          color: "#A770FF",
          highlight: "#9670FF",
          label: "5+ phones"
      }
    ]

    var ctx = $("#q3Chart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);
    var q3Chart = new Chart(ctx).Pie(q3Data, {});
  });

  $('.q4 .button').click(function(b) {

    $('#q4Chart').fadeIn('slow');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {
      $('.q5').delay(2000).slideDown('slow', function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      });
    });

    var q4Data = [
        {
            value: 20,
            color:"#46BFBD",
            highlight: "#5AD3D1",
            label: "Yes"
        },
        {
            value: 81,
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "No"
        }
    ]

    var ctx = $("#q4Chart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);
    var q4Chart = new Chart(ctx).Pie(q4Data, {});


  });

  $('.q5 .button').click(function(b) {

    $('#q5Chart').fadeIn('slow');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {
      $('.q6').delay(2000).slideDown('slow', function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      });
    });

    var q5Data = [
      {
          value: 1,
          color:"#F7464A",
          highlight: "#FF5A5E",
          label: "<1 year"
      },
      {
          value: 31,
          color: "#46BFBD",
          highlight: "#5AD3D1",
          label: "1-2 years"
      },
      {
          value: 34,
          color: "#FDB45C",
          highlight: "#FFC870",
          label: "2-3 years"
      },
      {
          value: 35,
          color: "#5CDFFD",
          highlight: "#70FFF5",
          label: "3+ years"
      }
    ]

    var ctx = $("#q5Chart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);
    var q5Chart = new Chart(ctx).Pie(q5Data, {});
  });

  $('.q6 .button').click(function(b) {

    $('#q6Chart').fadeIn('slow');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {
      $('.q7').delay(2000).slideDown('slow', function() {
        $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      });
    });

    var q6Data = [
      {
          value: 11,
          color:"#F7464A",
          highlight: "#FF5A5E",
          label: "Positive impact"
      },
      {
          value: 31,
          color: "#46BFBD",
          highlight: "#5AD3D1",
          label: "Positive & negative impact"
      },
      {
          value: 25,
          color: "#FDB45C",
          highlight: "#FFC870",
          label: "Negative impact"
      },
      {
          value: 22,
          color: "#5CDFFD",
          highlight: "#70FFF5",
          label: "Impact, but not specified"
      },
      {
          value: 10,
          color: "#A770FF",
          highlight: "#9670FF",
          label: "No impact"
      }
    ]

    var ctx = $("#q6Chart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);
    var q6Chart = new Chart(ctx).Pie(q6Data, {});
  });

});
